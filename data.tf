data "aws_vpc" "default_vpc" {
  id = var.existing_vpc_id
}

data "aws_subnet" "default_subnet1" {
  id = var.existing_subnet1_id
}

data "aws_subnet" "default_subnet2" {
  id = var.existing_subnet2_id
}

data "aws_iam_role" "taskExecutionRole" {
  name = "ecsTaskExecutionRole"
}