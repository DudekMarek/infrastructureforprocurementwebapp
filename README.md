# Opis infrastruktury AWS dla projektu [ProcurementInPolandWebApp](https://gitlab.com/DudekMarek/ProcurementInPolandWebApp)
---
1. Do stworzenia infrastruktury wykorzystano istniejącą już __VPC__, która została zaimportowan w pliki _data.tf_

2. Następnie zdefiniowano __Security groups__ dla:
    - __Elastic load balancer__ - zezwolenie na ryuch przychodzący na porcie 80 oraz zezwolenie na ruch wychodzący na każdym porcie
    - agent __Telegraf__ - zezwolenie na rych wchodzący i wychodzący
    - Task na którym działa aplikacja - zezwolenie na ruch wchodzący jedynie od __Elastic load balancer__ i agenta __Telegraf__

3. Zdefiniowanie __Task definition__ uruchamiającego w kontenery
    - pierwszy to kontener z aplikacją, który mapuje 2 porty 80 (działanie aplikacji) i 9090 (wystawianie metryk)
    - drugi kontener zawiera __Fluent Bit__ który zbiera logi z aplikacji i wysyła je do __Loki__ na __Grafana Cloud__

4. Zdefiniowanie __Elastic load balancer__ 
    - zdefiniowano _aws_lb_ typu _application_ który działa w 2 _subnet_
    - zdefiniowano _aws_lb_target_group_
    - zdefiniowano _aws_lb_listener_ który przekierowuje rych z internetu do _aws_lb_target_group_

5. Zdefiniowanie __ECS Cluster__

6. Zdefiniowanie __ECS Service__ który uruchamia odpowiednie __Task definition__
    - przypisanie serwisu do odpowiedniego __ECS Cluster__ 
    - przypisanie do serwisu dopowiedniego __Task definition__
    - przypisanie odpowiedniej __Security group__
    - przypisanie odpowiedniego __Elastic load balancer__