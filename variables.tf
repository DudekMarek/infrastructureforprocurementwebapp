variable "aws_profile" {
  type    = string
  default = "default"
}

variable "aws_default_region" {
  type    = string
  default = "eu-north-1"
}

variable "existing_vpc_id" {
  type        = string
  description = "ID of default VPC for ECS"
  default     = "vpc-04090798616512c79"
}

variable "existing_subnet1_id" {
  type        = string
  description = "ID of default subnet1 for ECS"
  default     = "subnet-01e02886930fde497"
}

variable "existing_subnet2_id" {
  type        = string
  description = "ID of default subnet2 for ECS"
  default     = "subnet-06e4867da5b4625cf"
}

variable "procurment_app_image_url" {
  type        = string
  description = "Url of image of procurment web app"
  default     = "296083653718.dkr.ecr.eu-north-1.amazonaws.com/procurement_web_app:b2538561"
}

variable "log_group" {
  type        = string
  description = "Log group"
  default     = "loki-test"
}

variable "loki_url" {
  type        = string
  description = "Url for pushing logs to loki"
  default     = "https://588967:eyJrIjoiOTljOWEzMDMzYjhlNTVhNmIzOWEwYzQ1YzMzZTY5NTA3NGM2OTEzNyIsIm4iOiJsb2dzIiwiaWQiOjg0OTM1OH0=@logs-prod-012.grafana.net/loki/api/v1/push"
}

variable "health_check_path" {
  type        = string
  description = "Path for health check"
  default     = "/health-check-endpoint"
}

variable "cluster_name" {
  type        = string
  description = "Name of clustes"
  default     = "procurment-app-cluster"
}