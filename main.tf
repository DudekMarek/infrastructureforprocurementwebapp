terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  profile = var.aws_profile
  region  = var.aws_default_region
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "${var.cluster_name}-ecsTaskExecutionRole"

  assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "ecs-tasks.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_security_group" "elastic_load_balancer_sg" {
  name        = "ElasticLoadBalancer_sg"
  description = "Security group for load balancer"
  vpc_id      = data.aws_vpc.default_vpc.id

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "telegraf_sg" {
  name        = "Telegraf-sg"
  description = "Security group for telegraf agent"
  vpc_id      = data.aws_vpc.default_vpc.id


  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = -1
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "task_sg" {
  name        = "Task_sg"
  description = "Security group for tasks"
  vpc_id      = data.aws_vpc.default_vpc.id


  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.elastic_load_balancer_sg.id, aws_security_group.telegraf_sg.id]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_ecs_task_definition" "procurment_app_task_definition" {
  family                   = "${var.cluster_name}-task"
  execution_role_arn       = data.aws_iam_role.taskExecutionRole.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512

  container_definitions = jsonencode([
    {
      name      = "procurment-app-container"
      essential = true
      image     = var.procurment_app_image_url
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
          protocol      = "tcp"
        },
        {
          containerPort = 9090
          hostPort      = 9090
          protocol      = "tcp"
        },
      ]
      logConfiguration = {
        logDriver = "awsfirelens",
        options = {
          Name       = "grafana-loki",
          Url        = var.loki_url,
          Labels     = "{job=\"firelens\"}",
          RemoveKeys = "container_id,ecs_task_arn",
          LabelKeys  = "container_name,ecs_task_definition,source,ecs_cluster",
          LineFormat = "key_value",
        }
      }
    },
    {
      name      = "log_router"
      image     = "grafana/fluent-bit-plugin-loki"
      essential = true
      firelensConfiguration = {
        type = "fluentbit",
        options = {
          enable-ecs-log-metadata = "true",
        }
      }
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = var.log_group,
          awslogs-region        = var.aws_default_region,
          awslogs-stream-prefix = "firelens",
        }
      }
    },
  ])

}

resource "aws_lb" "procurement_app_load_balancer" {
  name                       = "procurment-app-load-balancer"
  internal                   = false
  load_balancer_type         = "application"
  subnets                    = [data.aws_subnet.default_subnet1.id, data.aws_subnet.default_subnet2.id]
  security_groups            = [aws_security_group.elastic_load_balancer_sg.id]
  enable_deletion_protection = false
}

resource "aws_lb_target_group" "procurement_app_target_group" {
  name        = "procurment-app-target-group"
  target_type = "ip"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default_vpc.id

  health_check {
    healthy_threshold   = "3"
    unhealthy_threshold = "2"
    interval            = "30"
    protocol            = "HTTP"
    timeout             = "5"
    path                = var.health_check_path
    matcher             = "200"
  }
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.procurement_app_load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.procurement_app_target_group.arn
    type             = "forward"
  }
}

resource "aws_ecs_cluster" "procurement_app_cluster" {
  name = var.cluster_name
}

resource "aws_ecs_service" "procurement_app_service" {
  name                               = "procurment-app-service"
  cluster                            = aws_ecs_cluster.procurement_app_cluster.id
  task_definition                    = aws_ecs_task_definition.procurment_app_task_definition.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = [aws_security_group.task_sg.id]
    subnets          = [data.aws_subnet.default_subnet1.id, data.aws_subnet.default_subnet2.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.procurement_app_target_group.arn
    container_name   = "procurment-app-container"
    container_port   = 80
  }


}